import unittest
from tile_group import TileGroup
from tile import Tile
from word import Word
from tile_not_in_group_exception import TileNotInGroupException

#tests for the TileGroup#__init__ method
class TestTileGroup__init__(unittest.TestCase):
	
	#tests that the tile group is empty on init
	def test_should_be_empty_on_init(self):
		tilegroup = TileGroup()
		self.assertEqual(len(tilegroup.get_tiles()), 0)
		
		
#tests for the TileGroup#append method
class TestTileGroupAppend(unittest.TestCase):

	#tests when one tile is appended to tile group
	def test_append_one_tile(self):
		tilegroup = TileGroup()
		tile = Tile("A")
		tilegroup.append(tile)
		self.assertEqual(len(tilegroup.get_tiles()), 1)
		self.assertEqual(tilegroup.hand(), "A")
		
	#tests when many tiles are appended to tile group
	def test_append_many_tiles(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("A"))
		tilegroup.append(Tile("L"))
		tilegroup.append(Tile("I"))
		tilegroup.append(Tile("E"))
		tilegroup.append(Tile("N"))
		self.assertEqual(len(tilegroup.get_tiles()), 5)
		self.assertEqual(tilegroup.hand(), "ALIEN")
		
	#tests when duplicate tiles are appended to tile group
	def test_append_duplicate_tiles(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("P"))
		tilegroup.append(Tile("I"))
		tilegroup.append(Tile("Z"))
		tilegroup.append(Tile("Z"))
		tilegroup.append(Tile("A"))
		self.assertEqual(len(tilegroup.get_tiles()), 5)
		self.assertEqual(tilegroup.hand(), "PIZZA")
		
		
#tests for the TileGroup#remove method
class TestTileGroupRemove(unittest.TestCase):

	#tests when a tile cannot be removed from empty tile group
	def test_cannot_remove_from_empty_tilegroup(self):
		with self.assertRaises(TileNotInGroupException):
			tilegroup = TileGroup()
			tilegroup.remove(Tile("A"))
			
	#tests when a tile cannot be removed because it is not in tile group
	def test_cannot_remove_tile_not_in_tilegroup(self):
		with self.assertRaises(TileNotInGroupException):
			tilegroup = TileGroup()
			tilegroup.append(Tile("C"))
			tilegroup.append(Tile("A"))
			tilegroup.append(Tile("T"))
			tilegroup.append(Tile("S"))
			tilegroup.remove(Tile("D"))
			
	#tests when a tile is removed from tile group when only tile
	def test_can_remove_only_tile(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("A"))
		tilegroup.remove(Tile("A"))
		self.assertEqual(len(tilegroup.get_tiles()), 0)
	
	#tests when the first tile is removed from many
	def test_remove_first_tile_from_many(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("Z"))
		tilegroup.append(Tile("E"))
		tilegroup.append(Tile("U"))
		tilegroup.append(Tile("S"))
		tilegroup.remove(Tile("Z"))
		self.assertEqual(len(tilegroup.get_tiles()), 3)
		self.assertEqual(tilegroup.hand(), "EUS")
		
	#tests when last tile is removed from many	
	def test_can_remove_last_tile_from_many(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("H"))
		tilegroup.append(Tile("E"))
		tilegroup.append(Tile("R"))
		tilegroup.append(Tile("A"))
		tilegroup.remove(Tile("A"))
		self.assertEqual(len(tilegroup.get_tiles()), 3)
		self.assertEqual(tilegroup.hand(), "HER")
		
	#tests when middle tile is removed from many
	def test_can_remove_middle_tile_from_many(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("A"))
		tilegroup.append(Tile("R"))
		tilegroup.append(Tile("T"))
		tilegroup.append(Tile("E"))
		tilegroup.append(Tile("M"))
		tilegroup.append(Tile("I"))
		tilegroup.append(Tile("S"))
		tilegroup.remove(Tile("E"))
		self.assertEqual(len(tilegroup.get_tiles()), 6)
		self.assertEqual(tilegroup.hand(), "ARTMIS")
		
	#tests when multiple tiles are removed from tilegroup
	def test_can_remove_multiple_tiles(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("T"))
		tilegroup.append(Tile("H"))
		tilegroup.append(Tile("O"))
		tilegroup.append(Tile("R"))
		tilegroup.remove(Tile("O"))
		tilegroup.remove(Tile("R"))
		self.assertEqual(len(tilegroup.get_tiles()), 2)
		self.assertEqual(tilegroup.hand(), "TH")
		
	#tests that only one tile is removed when there are duplicate tiles
	def test_does_not_remove_duplicate_tiles(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("A"))
		tilegroup.append(Tile("T"))
		tilegroup.append(Tile("H"))
		tilegroup.append(Tile("E"))
		tilegroup.append(Tile("N"))
		tilegroup.append(Tile("A"))
		tilegroup.remove(Tile("A"))
		self.assertEqual(len(tilegroup.get_tiles()), 5)
		self.assertEqual(tilegroup.hand(), "THENA")
		
#tests for the TileGroup#hand method
class TestTileGroupHand(unittest.TestCase):

	#tests when the hand is empty
	def test_should_have_empty_hand(self):
		tilegroup = TileGroup()
		self.assertEqual(tilegroup.hand(), "")
		
	#tests when one tile is in hand
	def test_should_have_one_tile_in_hand(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("A"))
		self.assertEqual(tilegroup.hand(), "A")
		
	#tests when multiple tiles are in hand
	def test_should_have_many_tiles_in_hand(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("S"))
		tilegroup.append(Tile("P"))
		tilegroup.append(Tile("A"))
		tilegroup.append(Tile("C"))
		tilegroup.append(Tile("E"))
		self.assertEqual(tilegroup.hand(), "SPACE")
		
	#tests when multiple tiles are in hand with duplicates
	def test_should_have_many_tiles_in_hand_including_duplicates(self):
		tilegroup = TileGroup()
		tilegroup.append(Tile("S"))
		tilegroup.append(Tile("C"))
		tilegroup.append(Tile("U"))
		tilegroup.append(Tile("L"))
		tilegroup.append(Tile("L"))
		tilegroup.append(Tile("Y"))
		self.assertEqual(tilegroup.hand(), "SCULLY")
		
#tests for the Word#score method
class TestWordScore(unittest.TestCase):

	#tests that an empty word should have a score of 0
	def test_empty_word_should_have_score_of_zero(self):
		word = Word()
		self.assertEqual(word.score(), 0)
		
	#tests a score of a one tile word
	def test_score_a_one_tile_word(self):
		word = Word()
		word.append(Tile("S"))
		self.assertEqual(word.score(), 1)
		
	#tests a score with multiple different tiles
	def test_score_a_word_with_multiple_different_tiles(self):
		word = Word()
		word.append(Tile("S"))
		word.append(Tile("P"))
		word.append(Tile("A"))
		word.append(Tile("C"))
		word.append(Tile("E"))
		self.assertEqual(word.score(), 9)
		
	#tests a score with multiple recurring tiles
	def test_score_a_word_with_recurring_tiles(self):
		word = Word()
		word.append(Tile("A"))
		word.append(Tile("P"))
		word.append(Tile("O"))
		word.append(Tile("L"))
		word.append(Tile("L"))
		word.append(Tile("O"))
		self.assertEqual(word.score(), 8)
		