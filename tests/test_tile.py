import unittest
import sys
sys.path.append('../')
from tile import Tile
import string
#unit tests for the Tile#points method
class TestTilePoints(unittest.TestCase):
	
	#tests that an exception is thrown when no letter is passed as parameter in constructor
	def test_error_on_None(self):
		with self.assertRaises(TypeError):
			tile = Tile()
	
	#tests than an exception is thrown when there is an empty string
	def test_error_on_empty_string(self):
		with self.assertRaises(Exception):
			tile = Tile("")	
		
	#tests that a tile object can be made from all letters
	def test_letters(self):
		valid_letters = string.ascii_letters
		tile_list = []
		for x in valid_letters:
			tile = Tile(x)
			tile_list += tile.letter()
		result = ''.join(tile_list)
		self.assertEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ", result)
		
	#tests that a non-letter string is not allowed
	def error_on_non_letter(self):
		self.assertRaises(Exception, tile = Tile(":"))
		
	#tests that a multi-letter string is not allowed
	def error_on_too_many_letters(self):
		self.assertRaises(Exception, tile = Tile("ABC"))
	
	#checks appropriate tiles have a point value of 1	
	def test_one_point_tiles(self):
		one_point_tiles = [ "E", "A", "I", "O", "N", "R", "T", "L", "S", "U" ]
		sum = 0
		for x in one_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(10, sum)
		
	#checks appropriate tiles have a point value of 2
	def test_two_point_tiles(self):
		two_point_tiles = [ "D", "G" ]
		sum = 0
		for x in two_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(4, sum)
		
	#checks appropriate tiles have a point value of 3
	def test_three_point_tiles(self):
		three_point_tiles = [ "B", "C", "M", "P" ]
		sum = 0
		for x in three_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(12, sum)
		
	#checks appropriate tiles have a point value of 4
	def test_four_point_tiles(self):
		four_point_tiles = [ "F", "H", "V", "W", "Y" ]
		sum = 0
		for x in four_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(20, sum)
		
	#checks appropriate tiles have a point value of 5
	def test_five_point_tiles(self):
		tile = Tile("K")
		self.assertEqual(5, tile.points())
		
	#checks appropriate tiles have a point value of 8
	def test_eight_point_tiles(self):
		eight_point_tiles = [ "J", "X" ]
		sum = 0
		for x in eight_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(16, sum)
			
	#checks appropriate tiles have a point value of 2
	def test_ten_point_tiles(self):
		ten_point_tiles = [ "Q", "Z" ]
		sum = 0
		for x in ten_point_tiles:
			tile = Tile(x)
			sum += tile.points()
		self.assertEqual(20, sum)