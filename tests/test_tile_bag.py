import unittest
from tile import Tile
from tile_bag import TileBag
from empty_tilebag_exception import EmptyTileBagException
#unit tests for the TileBag#draw_tile method
class TestTileBagDrawTile(unittest.TestCase):
		
	#tests that exactly 98 tiles can be drawn
	def test_can_draw_all_tiles(self):
		tilebag = TileBag()
		list = []
		for x in range(0, 98):
			list.append(tilebag.draw_tile())
		self.assertTrue(tilebag.is_empty())
		self.assertEqual(len(list), 98)
		
	#tests that drawing the 99th tile causes the exception
	def test_can_not_draw_too_many_tiles(self):
		with self.assertRaises(EmptyTileBagException):
			tilebag = TileBag()
			for x in range(0,99):
				tilebag.draw_tile()


	#tests that the tiles in the bag are properly distributed
	def test_has_proper_tile_distribution(self):
		tilebag = TileBag().tilebag
		list = []
		for x in tilebag:
			list.append(x.letter())
		self.assertEqual(12, list.count("E"))
		self.assertEqual(9, list.count("A"))
		self.assertEqual(9, list.count("I"))
		self.assertEqual(8, list.count("O"))
		self.assertEqual(6, list.count("N"))
		self.assertEqual(6, list.count("R"))
		self.assertEqual(6, list.count("T"))
		self.assertEqual(4, list.count("L"))
		self.assertEqual(4, list.count("S"))
		self.assertEqual(4, list.count("U"))
		self.assertEqual(4, list.count("D"))
		self.assertEqual(3, list.count("G"))
		self.assertEqual(2, list.count("B"))
		self.assertEqual(2, list.count("C"))
		self.assertEqual(2, list.count("M"))
		self.assertEqual(2, list.count("P"))
		self.assertEqual(2, list.count("F"))
		self.assertEqual(2, list.count("H"))
		self.assertEqual(2, list.count("V"))
		self.assertEqual(2, list.count("W"))
		self.assertEqual(2, list.count("Y"))
		self.assertEqual(1, list.count("K"))
		self.assertEqual(1, list.count("J"))
		self.assertEqual(1, list.count("X"))
		self.assertEqual(1, list.count("Q"))
		self.assertEqual(1, list.count("Z"))		