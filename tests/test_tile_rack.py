import unittest
from tile import Tile
from tile_rack import TileRack
from tile_rack_full_exception import TileRackFullException
#unit tests for the TileRack#append method
class TestTileRackAppend(unittest.TestCase):

	#tests to make sure that more than 7 tiles cannot be appended to tilerack
	def test_should_not_append_more_than_max_tiles(self):
		with self.assertRaises(TileRackFullException):
			tilerack = TileRack()
			for x in range(0,8):
				tile = Tile("A")
				tilerack.append(tile)
				
				
#unit tests for the TileRack#number_of_tiles_needed method
class TestTileRackNumberOfTilesNeeded(unittest.TestCase):
	
	#tests that an empty tile rack should need max tiles
	def test_empty_tile_rack_should_need_max_tiles(self):
		tilerack = TileRack()
		self.assertEqual (7, tilerack.number_of_tiles_needed())
		
	#tests that a tile rack with one tile should need max minus one tiles
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("A"))
		self.assertEqual(6, tilerack.number_of_tiles_needed())
		
	#tests that a tile rack with several tiles should need some tiles
	def test_tile_rack_with_several_tiles_should_need_some_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("P"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("A"))
		self.assertEqual(2, tilerack.number_of_tiles_needed())
	
	#tests that a full tile rack needs 0 tiles
	def test_that_full_tile_rack_needs_zero_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("A"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("S"))
		tilerack.append(Tile("S"))
		self.assertEqual(0, tilerack.number_of_tiles_needed())
		
#unit tests for the TileRack#has_tiles_for method		
class TestTileRackHasTilesFor(unittest.TestCase):

	#tests that tile rack has needed tiles when letters are in order with no duplicates
	def test_rack_has_needed_tiles_when_letters_are_in_order_no_duplicates(self):
		tilerack = TileRack()
		tilerack.append(Tile("A"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("S"))
		tilerack.append(Tile("T"))
		self.assertEqual(True, tilerack.has_tiles_for("ALIENS"))
		
	#tests that tile rack has needed tiles when letters are not in order with no duplicates
	def test_rack_has_needed_tiles_when_letters_are_not_in_order_no_duplicates(self):
		tilerack = TileRack()
		tilerack.append(Tile("S"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("T"))
		self.assertEqual(True, tilerack.has_tiles_for("ALIENS"))
		
	#tests when the tile rack doesn't contain any needed tiles
	def test_rack_doesnt_contain_any_needed_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("S"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("T"))
		self.assertEqual(False, tilerack.has_tiles_for("DOG"))
	
	#tests when the tile rack contains some but not all needed tiles
	def test_rack_contains_some_but_not_all_needed_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("S"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("T"))
		self.assertEqual(False, tilerack.has_tiles_for("PIZZAS"))
		
	#tests when the rack contains a word with duplicate tiles
	def test_rack_contains_a_word_with_duplicate_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("P"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("T"))
		self.assertEqual(True, tilerack.has_tiles_for("PIZZA"))
	
	#tests when the rack doesn't contain enough duplicate tiles for word	
	def test_rack_doesnt_contain_enough_duplicate_tiles(self):
		tilerack = TileRack()
		tilerack.append(Tile("P"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("T"))
		self.assertEqual(False, tilerack.has_tiles_for("PIZZZA"))

#unit tests for the TileRack#remove_word method		
class TestTileRackRemoveWord(unittest.TestCase):

	#tests that a word can be removed whose letters are in order on the rack
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack(self):
		tilerack = TileRack()
		tilerack.append(Tile("A"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("S"))
		tilerack.append(Tile("T"))
		self.assertEqual("ALIENS", tilerack.remove_word("ALIENS").hand())
		self.assertEqual(6, tilerack.number_of_tiles_needed())
		self.assertEqual("T", tilerack.hand())
		
	#tests that a word whose letters are not in order on the rack can be removed
	def test_can_remove_word_whose_letters_are_not_in_order_on_the_rack(self):
		tilerack = TileRack()
		tilerack.append(Tile("T"))
		tilerack.append(Tile("L"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("S"))
		tilerack.append(Tile("N"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("A"))
		self.assertEqual("ALIENS", tilerack.remove_word("ALIENS").hand())
		self.assertEqual(6, tilerack.number_of_tiles_needed())
		self.assertEqual("T", tilerack.hand())
		
	#tests that a word can be removed with duplicate letters
	def test_can_remove_word_with_duplicate_letters(self):
		tilerack = TileRack()
		tilerack.append(Tile("P"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("A"))
		self.assertEqual("PIZZA", tilerack.remove_word("PIZZA").hand())
		self.assertEqual(5, tilerack.number_of_tiles_needed())
		self.assertEqual("EA", tilerack.hand())
		
	#tests that a word can be removed without removing unneeded duplicate letters
	def test_can_remove_word_without_removing_unneeded_duplicate_letters(self):
		tilerack = TileRack()
		tilerack.append(Tile("P"))
		tilerack.append(Tile("I"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("Z"))
		tilerack.append(Tile("A"))
		tilerack.append(Tile("E"))
		tilerack.append(Tile("A"))
		self.assertEqual("PIZA", tilerack.remove_word("PIZA").hand())
		self.assertEqual(4, tilerack.number_of_tiles_needed())
		self.assertEqual("ZEA", tilerack.hand())