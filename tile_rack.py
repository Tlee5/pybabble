from tile_group import TileGroup
from tile import Tile
from word import Word
from insufficient_tiles_exception import InsufficientTilesException
from tile_rack_full_exception import TileRackFullException
class TileRack(TileGroup):

	#returns the number of tiles needed to brin the rack back up to 7
	def number_of_tiles_needed(self):
		needed = 7 - len(super().get_tiles())
		return needed
		
	#returns true if the rack has enough tiles to make the word represented by the text parameter
	def has_tiles_for(self, text):
		tiles = super().hand()
		result = True
		for x in text:
			if x not in tiles:
				result = False
			else:
				tiles = tiles.replace( x, "", 1 )
		return result
		
	#returns a Word object made by removing the tiles given by text
	def remove_word(self, text):
		if self.has_tiles_for(text) == False:
			raise InsufficientTilesException("Not enough tiles to remove word")
		result = Word()
		for x in text:
			tile = Tile(x)
			result.append(tile)
			super().remove(tile)
		return result
		

		
	#overriden from the parent, throws TileRackFullException if an append is attempted while rack is full
	def append(self, tile):
		if len(super().get_tiles()) == 7:
			raise TileRackFullException("Cannot append tile because tile rack is full")
		super().append(tile)