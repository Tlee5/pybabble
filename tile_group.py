from tile import Tile
from tile_not_in_group_exception import TileNotInGroupException
#creates a container for multiple tile objects
class TileGroup:

	#creates the tile group
	def __init__(self):
		self.tiles = []
		
	#returns all tiles in the group
	def get_tiles(self):
		return self.tiles
		
	
	#appends a tile to the group
	def append(self, tile):
		if not isinstance(tile, Tile):
			raise Exception("Please input valid tile")
		self.tiles.append(tile)
	
	#removes a tile from the group
	def remove(self, tile):
		if tile in self.tiles:
			self.tiles.remove(tile)
		else:
			raise TileNotInGroupException("Cannot remove tile that is not in group")
	
	#returns a string that is the concatenation of all tiles letter values
	def hand(self):
		result = ""
		letters =list(map(lambda x: x.letter(), self.tiles))
		for x in letters:
			result += x
		return result
		
	
		