from tile_group import TileGroup
#represents a scoreable group of tiles
class Word(TileGroup):

	#returns the sum of all the tiles this word contains
	def score(self):
		tiles = super().get_tiles()
		sum = 0
		for x in tiles:
			sum += x.points()
		return sum
	