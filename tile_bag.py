from tile import Tile
from empty_tilebag_exception import EmptyTileBagException
import random
#represents a tilebag object
class TileBag:

	#constructor that creates a tilebag object
	def __init__(self):
		tiles = []
		tiles += 12 * ["E"]
		tiles += 9 * ["A"]
		tiles += 9 * ["I"]
		tiles += 8 * ["O"]
		for x in range(0,6):
			tiles += ["N"]
			tiles += ["R"]
			tiles += ["T"]
		for x in range(0,4):
			tiles += ["L"]
			tiles += ["S"]
			tiles += ["U"]
			tiles += ["D"]
		tiles += 3 * ["G"]
		for x in range(0,2):
			tiles += ["B"]
			tiles += ["C"]
			tiles += ["M"]
			tiles += ["P"]
			tiles += ["F"]
			tiles += ["H"]
			tiles += ["V"]
			tiles += ["W"]
			tiles += ["Y"]
		for x in range (0,1):
			tiles += ["K"]
			tiles += ["J"]
			tiles += ["X"]
			tiles += ["Q"]
			tiles += ["Z"]
		tilebag = list(map(lambda x: Tile(x), tiles))
		self.tilebag = tilebag
		
	#removes one random tile from the bag and returns it
	def draw_tile(self):
		if self.is_empty():
			raise EmptyTileBagException("Tilebag is empty")
		rand = random.SystemRandom()
		choice = rand.choice(self.tilebag)
		self.tilebag.remove(choice)
		return choice
		
	#returns true if the bag is empty, false if not
	def is_empty(self):
		if len(self.tilebag) == 0:
			return True
		else:
			return False
		
	