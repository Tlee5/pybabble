#exception class for if remove tile is called and tile is not present in group
class TileNotInGroupException(Exception):
	pass