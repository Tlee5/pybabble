#throws exception if the tile rack is full and an append is attempted
class TileRackFullException(Exception):
	pass