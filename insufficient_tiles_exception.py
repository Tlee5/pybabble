#exception class for if the tile rack does not have the needed tiles to remove
class InsufficientTilesException(Exception):
	pass