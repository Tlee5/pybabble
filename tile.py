#represents a tile with a letter and score
class Tile:

	#constructor that creates a tile object with given letter
	def __init__(self, letter):
		if not isinstance(letter, str) or not letter:
			raise Exception("Please enter valid letter")
		if not letter.isalpha():
			raise Exception("Please enter valid letter")
		if len(letter) > 1:
			raise Exception("Only one letter can be used each turn")
		self.tile_letter = letter.upper()
	
	#returns the letter of the tile	
	def letter(self):
		return self.tile_letter
	
	#returns the point value for the tile	
	def points(self):
		tile_points = {"A" : 1, "B" : 3, "C" : 3, "D" : 2, "E" : 1,\
		"F" : 4, "G" : 2, "H" : 4, "I" : 1, "J" : 8, "K" : 5, "L" : 1,\
		"M" : 3, "N" : 1, "O" : 1, "P" : 3, "Q" : 10, "R" : 1, "S" : 1, \
		"T" : 1, "U" : 1, "V" : 4, "W" : 4, "X" : 8, "Y" : 4, "Z" : 10 }
		point_value = 0
		for key in tile_points:
			if key == self.letter():
				point_value = tile_points[key]
		return point_value
	
	#returns if the tile is equal in value to the specified other tile	
	def __eq__(self, other):
		if self.letter() == other.letter():
			return True
		else:
			return False
	
	#returns if the tile is not equal in value to the specified other tile		
	def __ne__(self, other):
		if self.letter() != other.letter():
			return True
		else:
			return False
			