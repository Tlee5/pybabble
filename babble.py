from tile import Tile
from tile_bag import TileBag
from tile_rack import TileRack
from word import Word

#driver class for babble
class Babble:

	#creates babble objects
	def __init__(self):
		self.tilerack = TileRack()
		self.tilebag = TileBag()
		
	#main program loop
	def run(self):
		total_score = 0
		while self.tilebag.is_empty() == False:
			refill = self.tilerack.number_of_tiles_needed()
			for x in range(0,refill):
				tile = self.tilebag.draw_tile()
				self.tilerack.append(tile)
			print(self.tilerack.hand())
			word_choice = input('Guess a word: ')
			if word_choice != ":quit":
				if self.tilerack.has_tiles_for(word_choice.upper()):
					word = Word()
					self.tilerack.remove_word(word_choice.upper())
					for x in word_choice:
						tile = Tile(x)
						word.append(tile)
					score = word.score()
					print("You made", word_choice.upper(), "for", score, "points!")
					total_score += score
				else:
					print("Not enough tiles")	
				print("Total Score :", total_score)
			else:
				quit()
		
def main():
	Babble().run()
if __name__ == "__main__":
	main()